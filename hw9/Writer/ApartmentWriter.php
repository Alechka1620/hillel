<?php
class ApartmentWriter{
    public function write(Apartment $tovar){
        return '
            <tr>
                <td>' . $tovar->title .'</td>
                <td>' . $tovar->type.'</td>
                <td>' . $tovar->address .'</td>
                <td>' . $tovar->price.'</td>
                <td>' . $tovar->description.'</td>
                <td>' . $tovar->kitchen.'</td>
            </tr>
        ';
    }
}