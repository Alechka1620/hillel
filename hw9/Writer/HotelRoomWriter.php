<?php
class HotelRoomWriter{
    public function write(HotelRoom $tovar){
        return '
            <tr>
                <td>' . $tovar->title .'</td>
                <td>' . $tovar->type.'</td>
                <td>' . $tovar->address .'</td>
                <td>' . $tovar->price.'</td>
                <td>' . $tovar->description.'</td>
                <td>' . $tovar->roomNumber.'</td>
            </tr>
        ';
    }
}