<?php
class ShopTovarWriter{
    public function write(EstateObject $tovar){
        return '
            <tr>
                <td>' . $tovar->title .'</td>
                <td>' . $tovar->type.'</td>
                <td>' . $tovar->address .'</td>
                <td>' . $tovar->price.'</td>
                <td>' . $tovar->description.'</td>
            </tr>
        ';
    }
}