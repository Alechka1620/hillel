<?php 
    require_once $_SERVER['DOCUMENT_ROOT'].'/hw9/data.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/hw9/class/EstateObject.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/hw9/Writer/ShopTovarWriter.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/hw9/class/HotelRoom.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/hw9/class/Apartment.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/hw9/class/House.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/hw9/Writer/ApartmentWriter.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/hw9/Writer/HotelRoomWriter.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/hw9/Writer/HouseWriter.php';


if(!empty($_GET['product_id']) || $_GET['product_id'] == 0){
    $productId = $_GET['product_id'];
}else{
    header('Location:/');
    die();
}

$tovar = $tovars[$productId];
$writer = new ShopTovarWriter();
switch($tovar['type']){
    case 'hotel_room':
        $tovarObjects = new HotelRoom ($tovar['title'], 
            $tovar['type'], $tovar['address'], 
            $tovar['price'], $tovar['description'], $tovar['roomNumber']);
        $writer = new HotelRoomWriter();
    break;
    case 'house':
        $tovarObjects = new House($tovar['title'], 
            $tovar['type'], $tovar['address'], 
            $tovar['price'], $tovar['description'], $tovar['roomsAmount']);
        $writer = new HouseWriter();
    break;
    default:
        $tovarObjects = new Apartment($tovar['title'], 
            $tovar['type'], $tovar['address'], 
            $tovar['price'], $tovar['description'], $tovar['kitchen']);
        $writer = new ApartmentWriter();
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
</head>

<body>
<table class="table" >
    <?=$writer->write($tovarObjects)?>
</table>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous">
    </script>
</body>

</html>