<?php
$tovars=[
    ['title' => 'Отель Днепр',
    'type' => 'hotel_room',
    'address' => 'Днепр, Яворницкого 113',
    'price' => '60',
    'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Adipisci exercitationem enim eius accusantium cumque blanditiis, natus amet dolor neque, magni unde quod. Non quas reiciendis ipsa voluptatem voluptates, unde dolores fuga quibusdam nemo, vel, doloremque voluptate. Commodi quidem aspernatur error ducimus vel labore hic? Deleniti libero, neque labore fugiat dolorum culpa, dolore veritatis nihil obcaecati facere, porro error commodi sint hic quibusdam dicta quo eum molestiae omnis itaque laudantium. Corporis, tempore doloremque quaerat, provident, distinctio magnam consequuntur tempora blanditiis porro nemo est repudiandae totam omnis similique ab dolores? Sapiente at, quod saepe odit obcaecati magni. Perferendis perspiciatis excepturi molestiae ex.',
    'roomNumber' => '33'],

    ['title' => ' Люкс апартаменты',
    'type' => 'apartment',
    'address' => 'Харьков, пл Конституции 22',
    'price' => '70',
    'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Excepturi itaque provident placeat id assumenda necessitatibus sed consequuntur possimus. Error corporis nobis tempora, dicta cupiditate iusto culpa quisquam ipsa ab eligendi. Soluta veniam ullam dolores laboriosam officia, ducimus aut nihil deleniti rem facilis, ipsam explicabo sint beatae, cum blanditiis atque pariatur. Provident saepe ducimus fugit, qui autem atque cum facere. Mollitia omnis laudantium reiciendis rerum cumque.' ,
    'kitchen' =>true],

    ['title' => 'Квартира',
    'type' => 'house',
    'address' => 'Харьков, пл Конституции 122',
    'price' => '80',
    'description' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Quaerat, commodi at, sequi, eum minus labore nam libero tempore sit culpa officiis qui eaque quos sapiente minima. Minima, officia ab animi cum nulla corporis nobis totam libero deserunt eos pariatur odit beatae quod explicabo, omnis cupiditate possimus qui asperiores doloribus quibusdam voluptatem quas magni recusandae? Ipsam rem praesentium laudantium cum repellat eius vitae sapiente consequatur nobis.' ,
    'roomsAmount'=>'1 комната'],

    ['title' => 'Квартира',
    'type' => 'house',
    'address' => 'Одесса, Академика Королева 55, кв 37',
    'price' => '90',
    'description' => 'Lorem, ipsum dolor sit amet consectetur adipisicing elit. Tenetur officiis perferendis eveniet? Sed, veritatis! Voluptates nemo modi maxime eum quos culpa iusto quisquam amet, non est earum debitis in id expedita numquam temporibus, hic nulla perferendis accusantium. Nisi ratione velit repellat blanditiis a rerum neque modi aspernatur dignissimos veritatis excepturi cumque amet suscipit odio ex ea quas similique eaque officia repellendus earum eius, magnam obcaecati.'  ,
    'roomsAmount'=>'2' ],

    ['title' => 'Люкс апартаменты',
    'type' => 'apartment',
    'address' => 'Одесса, Академика Королева 155',
    'price' => '100',
    'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Quibusdam voluptatum, iste ab eaque vel quis repudiandae fuga nam tempore provident explicabo voluptatibus eius repellat officiis eum, quidem ullam voluptas reiciendis sed vero aliquid esse libero. Quisquam error fugit optio, debitis quasi eum eaque delectus hic distinctio inventore, beatae blanditiis itaque excepturi ipsam. Temporibus aut nam quaerat neque quam officiis. Pariatur corrupti odit odio recusandae nostrum!' ,
    'kitchen' =>'true' ],

    ['title' => 'Отель Днепр',
    'type' => 'hotel_room',
    'address' => 'Днепр, Яворницкого 213',
    'price' => '110',
    'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Officiis aspernatur eveniet ducimus nostrum est provident? Eos assumenda quas quia obcaecati, officiis temporibus laboriosam repudiandae ex quasi, magni ad explicabo est deleniti distinctio vel rerum delectus quis, dolorum aspernatur earum? Eligendi ea saepe, tenetur minima, laudantium natus possimus corrupti recusandae sunt dolorem blanditiis iusto beatae obcaecati dolorum quae, cumque est fugiat reiciendis ex dicta? Suscipit, possimus!' ,
    'roomNumber' => '66'],

    ['title' => 'Люкс апартаменты',
    'type' => 'apartment',
    'address' => 'Львов, ул Вернадского 55, кв 13',
    'price' => '120',
    'description' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Non magni itaque a ex nesciunt sapiente temporibus dicta inventore rem reprehenderit maiores nisi recusandae, eveniet iure soluta veritatis. Laboriosam quidem, modi pariatur, voluptate necessitatibus iure consequuntur ducimus praesentium quam, distinctio hic laudantium magni rem blanditiis officia. Fugiat, dolore laboriosam quidem ipsam molestiae et, nulla cum deserunt, dolorem voluptate assumenda delectus ea animi fugit dolorum iusto culpa!' ,
    'kitchen' =>false ],

    ['title' => 'Отель Прибережный',
    'type' => 'hotel_room',
    'address' => 'Днепр, Яворницкого 113',
    'price' => '130',
    'description' => 'описание',
    'roomNumber' => '55'],

    ['title' => 'Квартира в центре',
    'type' => 'house',
    'address' => 'Кривой Рог, ул Укринская 98, кв 44',
    'price' => '140',
    'description' => 'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Dicta debitis recusandae ad tenetur deleniti laborum. Voluptatum dolorum at odit numquam dolores itaque nesciunt aut consequatur. Amet corrupti impedit a hic adipisci veniam? Aliquid eum sint nesciunt distinctio enim modi culpa cumque quasi, obcaecati praesentium, placeat eveniet numquam exercitationem temporibus. Magni praesentium commodi nesciunt! Harum tempora sint iste asperiores neque, aspernatur et commodi beatae sapiente obcaecati.' ,
    'roomsAmount'=>'3' ],

    ['title' => 'Квартира',
    'type' => 'house',
    'address' => 'Киев, ул Васильковская 113, кв 35',
    'price' => '150',
    'description' => 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Id debitis rerum maiores vero similique ullam ipsa nisi non reiciendis expedita officiis sed voluptatum voluptatem eaque alias, porro laborum quasi perferendis, exercitationem delectus, voluptas adipisci? Est, tempora sequi, perspiciatis temporibus magnam recusandae porro rerum praesentium tempore, possimus autem placeat laborum non. Numquam saepe iste ex id modi optio voluptatibus eius. Corporis totam distinctio quo fuga assumenda.' ,
    'roomsAmount'=>'4'
    ],
];
?>