<?php 
class House extends EstateObject{
    public $roomsAmount = 0;
    public function __construct($title, $type, $address, $price, $description, $roomsAmount ){
        parent::__construct($title, $type, $address, $price, $description  );
        $this->roomsAmount=$roomsAmount;
    }
    
    public function getSummaryLine() {
        return  '<ul>' . parent::getSummaryLine() .'<li>' . $this->roomsAmount .'</li>' . '</ul>' ;
}
}