<?php 
class Apartment extends EstateObject{
    public $kitchen = 0;
    public function __construct($title, $type, $address, $price, $description, $kitchen ){
        parent::__construct($title, $type, $address, $price, $description  );
        $this->kitchen=$kitchen;
    }
    
    public function getSummaryLine() {
        return  '<ul>' . parent::getSummaryLine() .'<li>' . $this->kitchen .'</li>' . '</ul>' ;
}
}