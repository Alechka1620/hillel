<?php 
class HotelRoom extends EstateObject{
    public $roomNumber = 0;
    public function __construct($title, $type, $address, $price, $description, $roomNumber ){
        parent::__construct($title, $type, $address, $price, $description  );
        $this->roomNumber=$roomNumber;
    }
    
    public function getSummaryLine() {
        return  '<ul>' . parent::getSummaryLine() .'<li>' . $this->roomNumber .'</li>' . '</ul>' ;
}
}
?>
