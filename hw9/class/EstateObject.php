<?php 
class EstateObject {
    public $title;
    public $type;
    public $address;
    public $price;
    public $description;
    function __construct($title, $type, $address, $price, $description  )
    {  
        $this->title=$title;
        $this->type=$type;
        $this->address=$address;
        $this->price=$price;
        $this->description=$description;
    }
    public function getSummaryLine() {
        return '<li>' . $this->title .'</li>'
                    .'<li>' . $this->type .'</li>'
                    .'<li>' . $this->address .'</li>'
                    .'<li>' . $this->price .'</li>'
                    .'<li>' . $this->description .'</li>';

    }
}
?>