<?php
    require_once $_SERVER['DOCUMENT_ROOT'].'/hw9/data.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/hw9/class/EstateObject.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/hw9/class/HotelRoom.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/hw9/class/Apartment.php';
    require_once $_SERVER['DOCUMENT_ROOT'].'/hw9/class/House.php';

//$trty= new House ('jjj','dgsd', 'hdnj', 'rhzrh', 'thzdt', 'thzd');
//echo $trty->getSummaryLine();
//exit;

$tovarObjects = [];
foreach ($tovars as $tovar){
    switch($tovar['type']){
        case 'hotel_room':
            $tovarObjects[]= new HotelRoom ($tovar['title'], 
                $tovar['type'], $tovar['address'], 
                $tovar['price'], $tovar['description'], $tovar['roomNumber']);
        break;
        case 'house':
            $tovarObjects[]= new House($tovar['title'], 
                $tovar['type'], $tovar['address'], 
                $tovar['price'], $tovar['description'], $tovar['roomsAmount']);
        break;
        default:
            $tovarObjects[]= new Apartment($tovar['title'], 
                $tovar['type'], $tovar['address'], 
                $tovar['price'], $tovar['description'], $tovar['kitchen']);
    }

}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Homework</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <div class="row">
            <table class="table col-sm-12">
                <thead>
                    <tr>
                        <th scope="col">Название</th>
                        <th scope="col">Тип</th>
                        <th scope="col">Цена</th>
                        <th scope="col">Описание</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($tovars as $key => $tovar): ?>
                <tr>
                        <td scope="col"><?= $tovar['title']?></td>
                        <td scope="col"><?= $tovar['type']?></td>
                        <td scope="col"><?= $tovar['price']?></td>
                        <td scope="col"><a href="/hw9/show.php?product_id=<?=(string)$key ?>"> Описание</td>
                </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

    

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous">
    </script>

</body>
  
</html>