<?php
session_start();
if (empty ($_POST['valuta'])&& (empty ($_SESSION['valuta']))){
    $_SESSION['valuta']='uah'; 
}
elseif(!empty ($_POST['valuta'])) {
    $_SESSION['valuta']=
    $_POST['valuta'];
}


/*if (!empty ($_POST['valuta'])){
    if($_POST['valuta']==='uah'){
        $_SESSION['valuta']='uah';
    }
    if($_POST['valuta']==='usd'){
        $_SESSION['valuta']='usd';
    }
    if($_POST['valuta']==='eur'){
        $_SESSION['valuta']='eur';
    };
}
elseif (empty ($_POST['valuta'])&& (empty ($_SESSION['valuta']))){
        $_SESSION['valuta']='uah'; 
}
*/ 


/*
session_start();
// $_SESSION['valuta']='uah';
if (!empty ($_POST['valuta'])&& $_POST['valuta']==='uah'){
    $_SESSION['valuta']='uah';}
elseif (!empty ($_POST['valuta'])&& $_POST['valuta']==='usd'){
    $_SESSION['valuta']='usd';
}
elseif (!empty ($_POST['valuta'])&& $_POST['valuta']==='eur'){
    $_SESSION['valuta']='eur';
}
elseif (empty ($_SESSION['valuta'])){
    $_SESSION['valuta']='uah';
}
 */   
    $valuta=[
        'uah'=>['name' => 'Гривна',
                'course' => 1,],
        'usd'=>['name' => 'Доллар',
                'course' => 27.1,],
        'eur'=> ['name' => 'Евро',
                'course' => 30.2,],
];

$course = $valuta[ $_SESSION['valuta'] ] [ 'course' ] ;
$tovars=[
        ['title' => 'Помидоры',  
        'price_val' => 22.2, ],
        ['title' => 'Огурцы',
        'price_val' => 33.3,],
        ['title' => 'Картофель',
        'price_val' => 12.2, ],
        ['title' => 'Свекла', 
        'price_val' =>  18.8,],
        ['title' => 'Морковь',
        'price_val' => 75.5,],
        ['title' =>  'Баклажаны',
        'price_val' => 27.7,],
        ['title' =>  'Яблоки', 
        'price_val' =>  31.1, ],
        ['title' => 'Груши', 
        'price_val' => 14.4, ],
        ['title' => 'Виноград',
        'price_val' =>  120, ],
        ['title' =>  'Сливы',
        'price_val' => 85.6,],
];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Homework </title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
</head>

<body>
<form action="index.php" method="POST">
    <div>
        <select class="form-select" aria-label="Default select example" name="valuta" value=<?=$_POST['valuta']?> >
            <option <?php if($_SESSION['valuta'] === 'uah'):?>selected<?php endif;?> value="uah">Гривна</option>
            <option <?php if($_SESSION['valuta'] === 'usd'):?>selected<?php endif;?> value="usd">Доллар</option>
            <option <?php if($_SESSION['valuta'] === 'eur'):?>selected<?php endif;?> value="eur">Евро</option>
        </select>
        <button type="submit" class="btn btn-primary">Отправить</button>
    </div>
    <h1><?=$_SESSION['valuta']?></h1>
    <div class="container">
        <div class="row">
            <table class="table col-sm-12">
                <thead>
                    <tr>
                        <th scope="col">Название товара</th>
                        <th scope="col">Цена товара</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($tovars as $tovar): ?>
                    <tr>
                        <th><?= $tovar['title']?></th>
                        <td><?= $tovar['price_val']/$course?></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous">
    </script>
</body>

</html>