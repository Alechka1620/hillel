<?php
$tasks=[
    ['title' => 'Купить хлеб',
    'completed' => false], 
    ['title' => 'Выбросить мусор',
    'completed' => true],
    ['title' => 'Выгулять собаку',
    'completed' => false],
    ['title' => 'Помыть посуду',
    'completed' => true],
    ['title' => 'Сходить в кино',
    'completed' => false],
    ['title' => 'Посетить занятие по английскому языку',
    'completed' => false],
];
if (isset($_POST['title'])){
    $tasks[]=['title' => $_POST['title'] , 'completed' => false ];
}
setcookie('tasks', json_encode($tasks), time()+60*60*24*365); 
header('Location:/hw10/index.php');
?>