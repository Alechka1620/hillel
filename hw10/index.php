<?php
// $task = null;
// if(!empty($_COOKIE['task'])){
//     $task = json_decode($_COOKIE['task'], true);
// }
// print_r($task);
// $task=[
//     ['title' => 'Текст задачи, например купить хлеб',
//     'completed' => 'false'], 
//     ['title' => 'Текст задачи, например купить хлеб',
//     'completed' => 'false '],
//     ['title' => 'Текст задачи, например купить хлеб',
//     'completed' => 'false' ],
//     ['title' => 'Текст задачи, например купить хлеб',
//     'completed' => 'false '],
// ]
// ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Homework</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <form action="addTask.php" method="POST">
                    <div class="mb-3">
                        <label for="exampleInputName" class="form-label">Задача для TODO листа</label>
                        <input type="name" class="form-control" id="exampleInputName" aria-describedby="NameHelp"
                            name="title" value="">
                    </div>
                    <button type="submit" class="btn btn-primary">Отправить</button>
                </form>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <table class="table col-sm-12">
                <thead>
                    <tr>
                        <th scope="col" name='item_id'>Порядковый номер</th>
                        <th scope="col">Задачи</th>
                        <th scope="col">Выполнена задача или нет</th>
                        <th scope="col">Выполнить</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(!empty($_COOKIE['tasks'])):
                            $tasks = json_decode($_COOKIE['tasks'], true)  ?>
                        <?php foreach ($tasks as $key=>$task): ?>
                            <tr>
                                <th><?= $key+1 ?></th>
                                <td> <?= $task['title'] ?></td>
                            <?php if($task['completed']===false): ?>
                                <td><?= "Не выполнено" ?></td>
                                <td>
                                    <form action="completeTask.php" method="POST">
                                    <input type="hidden" aria-describedby="PhoneHelp" name="number" value="<?= $key ?>" >
                                        <button type="submit" class="btn btn-primary" > Выполнить</button>
                                    </form>
                                </td>
                            <?php else:?>
                                <td><?= "Выполнено" ?></td>
                               
                            <?php endif; ?>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>


                    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
                        integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0"
                        crossorigin="anonymous">
                    </script>
</body>

</html>