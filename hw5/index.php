<?php
  $UAH=1000;
  $USD=29;
  $EUR=32;
  $tovary=[
                ['title' => 'pizza', 'price' =>20, 'category' => 'food'],
                ['title' => 'pasta', 'price' =>30, 'category' => 'food'],
                ['title' => 'borshch', 'price' =>40, 'category' => 'food'],
                ['title' => 'fish', 'price' =>50, 'category' => 'food'],
                ['title' => 'chiken', 'price' =>60, 'category' => 'food'],
                ['title' => 'pepsi', 'price' =>70, 'category' => 'drink'],
                ['title' => 'cola', 'price' =>80, 'category' => 'drink'],
                ['title' => 'sprite', 'price' =>90, 'category' => 'drink'],
                ['title' => 'vino', 'price' =>100, 'category' => 'drink'],
                ['title' => 'vodka', 'price' =>10, 'category' => 'drink']
  ];

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
</head>

<body>
    <div class="container">
        <div class="row">
            <table class="table col-sm-12">
                <thead>
                    <tr>
                        <th scope="col">Гривна</th>
                        <th scope="col">Доллар</th>
                        <th scope="col">Евро</th>
                    </tr>
                </thead>
                <tbody>
                    <?php  while($UAH<2000  ):?>
                        <tr>
                            <th><?= $UAH ?></th>
                            <td><?=   $USD*$UAH ?></td>
                            <td><?= $EUR*$UAH ?></td>
                        </tr>
                    <?php $UAH=$UAH+50; endwhile;?>

                    <div class="container">
                        <div class="row">
                            <table class="table col-sm-12">
                                <thead>
                                    <tr>
                                        <th scope="col">title</th>
                                        <th scope="col">price</th>
                                        <th scope="col">category</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($tovary as $tovar):?>
                                    <tr>
                                        <td><?= $tovar['title'] ?></td>
                                        <td><?= $tovar['price']*0.9 ?></td>
                                        <td> <?= $tovar['category'] ?></td>
                                    </tr>
                                    <?php endforeach; ?>

                                </tbody>
                            </table>

                            <script
                                src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
                                integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW"
                                crossorigin="anonymous">
                            </script>

</body>

</html>