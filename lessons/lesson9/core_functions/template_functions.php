<?php
/**
 * 
 * Сформировать и отобразить шаблон
 * $templateName - абсолютный путь к шаблону относительно каталога templates
 */
function template($templateName, $data = []){
    if(!empty($data['seo'])){
        $seo = $data['seo'];
    }else{
        $seo = [
            'title' => 'Website about lesson 9',
            'description' => 'A lot of usefull information'
        ];
    }
    include $_SERVER['DOCUMENT_ROOT'] . '/templates/' . $templateName.'.tmpl.php';
}