<?php
$firstName = 'John';
$lastName = 'Doe';
$bold = true;
function sayHello($userName, $userLastName, $bold = false, $title){
    $fullName = $title . ' ' . $userName . ' ' . $userLastName;
    if($bold){
        $fullName = '<strong>' . $fullName . '</strong>';
    }
    return 'Hello ' . $fullName . '!'; 
    
}

function giveMeAFive(){
    return 5;
}
//$value = 5, $roundValue = 2
function doSquare($value, $roundValue = 2){
    return round($value * $value, $roundValue);
   

$jsonFormatedArray = json_encode(['Alex', 'Bob', 'Jack']);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<p><?= sayHello('Alex', 'Sosnitsky', null ,'sir')?></p>
<p><?= sayHello('Jack', 'London', true, 'mr')?></p>
<p><?= sayHello('Jason', 'Momoa', null,'sir')?></p>
<p><?= sayHello($firstName, $lastName, $bold, 'doctor')?></p>

<p><?= giveMeAFive();?></p>

<p><?= doSquare(giveMeAFive(), 4); ?></p>
<div>
    <?php print_r([giveMeAFive(), 
        doSquare(giveMeAFive(), 4), 
        sayHello('Alex', 'Sosnitsky', null ,'sir')]);?>
</div>

<p><?php echo str_replace("Javascript", 'PHP', 'I love Javascript')?></p>


</body>
</html>




