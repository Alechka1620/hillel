<?php
    
    /* подключает файл сколько угодно раз, 
    если не находит - фатальная ошибка */
    //require $_SERVER['DOCUMENT_ROOT'].'/system_data/items.array.php';
    
    /* подключает файл только ОДИН раз, дальше игнорирует. 
    Если не находит - фатальная ошибка */
    require_once $_SERVER['DOCUMENT_ROOT'].'/system_data/items.array.php';

    $cart = null;
    if(!empty($_COOKIE['cart'])){
        $cart = json_decode($_COOKIE['cart'], true);
    }
    print_r($cart);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

    <?php include $_SERVER['DOCUMENT_ROOT'].'/templates/header_menu.php'?>
    <form method='post' action="/set_personal_data.php">
        <label for="">
            Enter your first name:
            <input type="text" name='firstName'>
        </label>

        <label for="">
            Enter your age:
            <input type="text" name='age'>
        </label>
        <button>Send</button>
    </form>
    <h2>Filter:</h2>
    <form action="/" method="get">
        <label>
            Select type:
            <select name="item_type">
                <option value='laptop'>Laptops</option>
                <option value='phone'>Phones</option>
            </select>
        </label>
        <input type="submit" value="Send">
    </form>

    <?php include_once $_SERVER['DOCUMENT_ROOT'].'/templates/mini_cart.php'?>

    <h2>Items:</h2>
    <ul>
        <?php if(!empty($_GET['item_type'])):?>
            <!-- SHOW ONLY FILTERED RESULTS BY TYPE -->
            <?php foreach($items as $itemId => $item):?>
                <?php if($_GET['item_type'] === $item['type']):?>
                    <li> 
                        <?=$item['title']?> - <?=$item['price']?> 
                        <form method="post" action="/add_to_cart.php">
                            <input type="hidden" name='item_id' value="<?=$itemId?>"> 
                            <button>Buy </button>
                        </form>
                </li>
                <?php endif?>
            <?php endforeach;?>
        <?php else:?>
            <!-- SHOW ALL -->
            <?php foreach($items as $itemId => $item):?>
                <li>
                    <?=$item['title']?> - <?=$item['price']?>
                    <form method="post" action="/add_to_cart.php">
                            <input type="hidden" name='item_id' value="<?=$itemId?>"> 
                            <button>Buy </button>
                        </form>
                </li>
            <?php endforeach;?>
        <?php endif; ?>
    </ul>
    <?php include $_SERVER['DOCUMENT_ROOT'].'/templates/footer.php'?>
</body>
</html>