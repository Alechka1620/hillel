<?php
// В куках был массив cart вида {'порядковый номер' : кол-во покупок }
// Если эллемент добавляется проверяем есть ли массив cart, если нет - создаем
// если есть - записываем в переменную 
// Проверяем а был ли куплен уже такой товар если да - увеличиваем кол-во на 1
// если нет создаем эллемент
// результат, массив cart записать обратно в cookie

if(!empty($_POST['item_id'])){

    $cart = [];
    $itemId = $_POST['item_id'];
    if(!empty($_COOKIE['cart'])){
        $cart = json_decode($_COOKIE['cart'], true);
    }

    if(!empty($cart[$itemId])){
        // $cart[$itemId] = $cart[$itemId] + 1;
        // $cart[$itemId] += 1;
        $cart[$itemId]++;
    }else{
        $cart[$itemId] = 1;
    }


    setcookie('cart', json_encode($cart), time()+60*60*24*2);
    header('Location:/');
}else{
    header('Location:/');
}