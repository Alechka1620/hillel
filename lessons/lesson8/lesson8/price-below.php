<?php
    require $_SERVER['DOCUMENT_ROOT'].'/system_data/items.array.php';
    $priceBelow = null;
    if(!empty($_GET['below'])){
        $priceBelow = (float) $_GET['below'];
    }
    $cart = null;
    if(!empty($_COOKIE['cart'])){
        $cart = json_decode($_COOKIE['cart'], true);
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<?php include $_SERVER['DOCUMENT_ROOT'].'/templates/header_menu.php'?>
<!-- <?php include_once $_SERVER['DOCUMENT_ROOT'].'/templates/header_menu.php'?> -->

<?php include_once $_SERVER['DOCUMENT_ROOT'].'/templates/mini_cart.php'?>
<h2>Items:</h2>
    <ul>

        <!-- SHOW ONLY FILTERED RESULTS BY TYPE -->
        <?php foreach($items as $item):?>
            <?php if($item['price'] <= $priceBelow):?>
                <li><?=$item['title']?> - <?=$item['price']?></li>
            <?php endif?>
        <?php endforeach;?>
    </ul>
    <?php include $_SERVER['DOCUMENT_ROOT'].'/templates/footer.php'?>
</body>
</html>