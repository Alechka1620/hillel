<?php if($cart):?>
    <h2>CART:</h2>
    <?php foreach($cart as $cartItemId => $amount):?>
        <p>
            <?=$items[$cartItemId]['title']?> 
        x <?=$amount?> 
        : <?=round($amount*$items[$cartItemId]['price'], 2)?> USD
    </p>
    <?php endforeach;?>
<?php endif?>