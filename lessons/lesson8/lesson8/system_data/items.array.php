<?php
$items = [
    [
        'title' => 'Macbook AIR',
        'price' => 900,
        'type' => 'laptop'
    ],
    [
        'title' => 'Iphone',
        'price' => 500,
        'type' => 'phone',
    ],
    [
        'title' => 'ASUS ASPIRE',
        'price' => 1100,
        'type' => 'laptop'
    ],
    [
        'title' => 'Samsung',
        'price' => 700,
        'type' => 'phone'
    ],
    [
        'title' => 'Xiaomy',
        'price' => 400,
        'type' => 'phone'
    ]
];
