<?php
$students = [
    [
        'firstName' => 'Alex',
        'lastName' => 'Sosnitsky',
        'course' => 'PHP',
        'avarangeMark' => 'Excellent'
    ],
    [
        'firstName' => 'Bob',
        'lastName' => 'Sosnitsky',
        'course' => 'HTML',
        'avarangeMark' => 'Good'
    ],
    [
        'firstName' => 'Jack',
        'lastName' => 'Sosnitsky',
        'course' => 'CSS',
        'avarangeMark' => 'Good'
    ],
    [
        'firstName' => 'Alex',
        'lastName' => 'Bush',
        'course' => 'PHP',
        'avarangeMark' => 'Bad'
    ],
    [
        'firstName' => 'Susan',
        'lastName' => 'Sosnitsky',
        'course' => 'PHP',
        'avarangeMark' => 'Excellent'
    ],
    [
        'firstName' => 'Ann',
        'lastName' => 'Doe',
        'course' => 'HTML',
        'avarangeMark' => 'Very Good'
    ],
    [
        'firstName' => 'Jennifer',
        'lastName' => 'Sinclar',
        'course' => 'CSS',
        'avarangeMark' => 'Bad'
    ],
    [
        'firstName' => 'Alex',
        'lastName' => 'Doe',
        'course' => 'PHP',
        'avarangeMark' => 'Excellent'
    ],
    [
        'firstName' => 'Peter',
        'lastName' => 'Doe',
        'course' => 'PHP',
        'avarangeMark' => ''
    ]
];
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Students search result</title>
</head>
<body>
    <ul>
        <?php if(!empty($_GET['firstName'])):?>
            <?php foreach($students as $student):?>
                <?php if($student['firstName'] === $_GET['firstName']):?>
                    <li>
                        <?=$student['firstName']?> <?=$student['lastName']?> 
                        - <?=$student['avarangeMark']?>
                    </li>
                <?php endif;?>
            <?php endforeach?>
        <?php else:?>
            <?php foreach($students as $student):?>
                <li>
                    <?=$student['firstName']?> <?=$student['lastName']?> 
                    - <?=$student['avarangeMark']?>
                </li>
            <?php endforeach?>
        <?php endif;?>
    </ul>
</body>
</html>