<?php
// 7 or '7'
$x = 10;
if(5 === $x){
    echo 'If FIVE than true and you see me!';
}elseif(10 === $x){
    echo 'WAHOO!!!';
}elseif(12 === $x){
    echo '12!!!';
}else{
   echo 'FALSE IN IF STATEMENT!'; 
}
echo '<br>';
echo 'after if';
echo '<br>';
$y = '7';
if($y !== 7){
    echo 'ITS NOT SEVEN!!!';
}

echo '<br>';
echo 'after if with NOT';

$item = [
    'title' => 'lemon',
    'price' => 2
];
echo '<br>';
if($item['title'] === 'lemon' && $item['price'] < 3){
    echo 'WOW CHEAP LEMON!';
}
echo '<br>';
$users = [
    'Alex',
    'Bobby'
];
if($users[0] === 'Alex' || $users[1] === 'Bob'){
    echo 'Secret: Password is FISH!';
}
echo '<br>';
if(!false){
    echo 'I work!';
}
echo '<br>';
if(!(1 == 2)){
    echo 'I work!!1 == 2';
}
echo '<br>';

$students = [
    [
        'firstName' => 'Alex',
        'lastName' => 'Sosnitsky',
        'course' => 'PHP',
        'avarangeMark' => 'Excellent'
    ],
    [
        'firstName' => 'Bob',
        'lastName' => 'Sosnitsky',
        'course' => 'HTML',
        'avarangeMark' => 'Good'
    ],
    [
        'firstName' => 'Jack',
        'lastName' => 'Sosnitsky',
        'course' => 'CSS',
        'avarangeMark' => 'Good'
    ],
    [
        'firstName' => 'John',
        'lastName' => 'Doe',
        'course' => 'PHP',
        'avarangeMark' => 'Bad'
    ],
    [
        'firstName' => 'Susan',
        'lastName' => 'Sosnitsky',
        'course' => 'PHP',
        'avarangeMark' => 'Excellent'
    ],
    [
        'firstName' => 'Ann',
        'lastName' => 'Doe',
        'course' => 'HTML',
        'avarangeMark' => 'Very Good'
    ],
    [
        'firstName' => 'Jennifer',
        'lastName' => 'Sinclar',
        'course' => 'CSS',
        'avarangeMark' => 'Bad'
    ],
    [
        'firstName' => 'Jack',
        'lastName' => 'Doe',
        'course' => 'PHP',
        'avarangeMark' => 'Excellent'
    ],
    [
        'firstName' => 'Peter',
        'lastName' => 'Doe',
        'course' => 'PHP',
        'avarangeMark' => ''
    ]
];

foreach($students as $student){
    if($student['course'] === 'PHP'){
        echo $student['firstName'] . ' ' . $student['lastName'] . '<br>';
    }
}

foreach($students as $student){
    if($student['avarangeMark'] === 'Excellent'){
        echo $student['firstName'] . ' ' . $student['lastName'] . ' - You are the best! <br>';
    }elseif($student['avarangeMark'] === 'Good'){
        echo $student['firstName'] . ' ' . $student['lastName'] . ' - You can do better! <br>';
    }elseif($student['avarangeMark'] === 'Very Good'){
        echo $student['firstName'] . ' ' . $student['lastName'] . ' - You are the best!! <br>';
    }elseif($student['avarangeMark'] === 'Bad'){
        echo $student['firstName'] . ' ' . $student['lastName'] . ' - Please go harder! <br>';
    }else{
        echo $student['firstName'] . ' ' . $student['lastName'] . ' - Waiting to your results! <br>';
    }
}

foreach($students as $student){
    switch($student['avarangeMark']){
        case 'Very Good':
        case 'Excellent':
            echo $student['firstName'] . ' ' . $student['lastName'] . ' - You are the best! <br>';
            break;
        case 'Good':
            echo $student['firstName'] . ' ' . $student['lastName'] . ' - You can do better! <br>';
            break;
        case 'Bad':
            echo $student['firstName'] . ' ' . $student['lastName'] . ' - Please go harder! <br>';
            break;
        default:
            echo $student['firstName'] . ' ' . $student['lastName'] . ' - Waiting to your results! <br>';
            break;
    }
}


