<?php
if(
    (!empty($_POST['note']))  
&& (!empty($_POST['color']))
&& (!empty($_POST['key']))
){
    session_start();
    $key = (int)$_POST['key'];
    $newNote = [
        'color' => $_POST['color'],
        'note' => $_POST['note']
    ];
    $_SESSION['notes'][$key] = $newNote;
    header('Location:/');
}