<?php
session_start();
$_SESSION['test'] = 'Hello world!';
if(!empty($_SESSION['notes'])){
    $notes = $_SESSION['notes'];
}else{
    $notes = NULL;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        .note{
            width:200px;
            min-height:100px;
            float:left;
            margin:20px;
            padding:10px;
        }
        .clearfix{
            clear:both;
        }
    </style>
</head>
<body>
    <h1>Session is set.</h1>
    <a href="/session_test.php">Check session array</a>
    <h2>Notepad:</h2>
    <div>
        <?php if($notes):?>
            <?php foreach($notes as $key => $note):?>
                <div class='note' style='background:<?=$note['color']?>;'>
                    <p><?=$note['note']?></p>
                    <a href="/edit_note.php?key=<?=$key?>">Edit</a>
                    <form action="note_destroy.php" method='post'>
                        <input type="hidden" name="key" value="<?=$key?>">
                        <button>Delete</button>
                    </form>
                </div>
                
            <?php endforeach;?>
            <div class="clearfix"></div>
        <?php else:?>
            <h4>Hey go and add some notes!</h4>
        <?php endif;?>
    </div>

    <h2>Add Note:</h2>
    <form action="add_note.php" method="post">
        <div>
            <label>
                Choose background color:
                <br>
                <select name="color">
                    <option value='red'>Red</option>
                    <option value="green">Green</option>
                    <option value="lightgrey">Grey</option>
                    <option value="lightyellow">Yellow</option>
                </select>
            </label>
        </div>
        <div>
            <label>
                Note text:
                <br>
                <textarea name="note"></textarea>
            </label>
        </div>
        <div>
            <button>Add note</button>
        </div>
    </form>
</body>
</html>