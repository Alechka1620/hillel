<?php
    session_start();
    if(!empty($_GET['key'])){
        $key = (int)$_GET['key'];
    }else{
        header('Location:/');
    }
    $note = $_SESSION['notes'][$key];

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<h2>Edit Note:</h2>
    <form action="update_note.php" method="post">
    <input type="hidden" name="key" value="<?=$key?>">
        <div>
            <label>
                Choose background color:
                <br>
                <select name="color">
                    <option <?php if($note['color'] === 'red'):?>selected<?php endif;?> value='red'>Red</option>
                    <option <?php if($note['color'] === 'green'):?>selected<?php endif;?> value="green">Green</option>
                    <option <?php if($note['color'] === 'lightgrey'):?>selected<?php endif;?> value="lightgrey">Grey</option>
                    <option <?php if($note['color'] === 'lightyellow'):?>selected<?php endif;?> value="lightyellow">Yellow</option>
                </select>
            </label>
        </div>
        <div>
            <label>
                Note text:
                <br>
                <textarea name="note"><?=$note['note']?></textarea>
            </label>
        </div>
        <div>
            <button>Update note</button>
        </div>
    </form>
</body>
</html>