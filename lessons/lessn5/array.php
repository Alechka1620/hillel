<?php
$titles = [
    'Php is awesome',
    'Html is awesome',
    'We love CSS',
    'We learn Mysql',
    'We love Regular Expressions',
    'Php is awesome',
    'Html is awesome',
    'We love CSS',
    'We learn Mysql',
    'We love Regular Expressions',
    'Php is awesome',
    'Html is awesome',
    'We love CSS',
    'We learn Mysql',
    'We love Regular Expressions',
    'We love CSS',
    'We love CSS',
    'We love CSS',
];
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Blog articles</h1>
    <h2>WHILE:</h2>
    <?php $i = 0; while($i <= 14):?>
    <h3> <?=$titles[$i];?> </h3>   
    <?php $i++; endwhile; ?>
    
    <h2>FOR:</h2>
    <?php for($z = 0;$z <= 14;$z++):?>
        <h3> <?=$titles[$z];?> </h3> 
    <?php endfor;?>

    <h2>FOREACH:</h2>
    <?php foreach($titles as $key => $title):?>
        <h3> <span style="color:red;"><?=$key?></span> <?=$title?></h3>
    <?php endforeach?>
</body>
</html>