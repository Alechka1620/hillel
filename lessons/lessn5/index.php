<?php
$titles = [
    'Php is awesome',
    'Html is awesome',
    'We love CSS',
    'We learn Mysql',
    'We love Regular Expressions',
    'Php is awesome',
    'Html is awesome',
    'We love CSS',
    'We learn Mysql',
    'We love Regular Expressions',
    'Php is awesome',
    'Html is awesome',
    'We love CSS',
    'We learn Mysql',
    'We love Regular Expressions',
];
$usd = 28;
$uahAmount = 100;
$uahMaximum = 500;
$iterator = 20;

//100 <= 500
while($uahAmount <= $uahMaximum){
    echo $uahAmount . ' - ' . round($uahAmount/$usd, 2);
    echo '<br>';
    $uahAmount = $uahAmount + $iterator;
}

echo 'last iteration result:' . $uahAmount;
echo '<br>';
echo 'END!';
echo '<br>';
echo 'NUMBERS:';
echo '<br>';
$start = 0;
while($start <= 10){
    echo 'number: ' . $start;
    echo '<br>';
    //$start = $start + 1;
    // $start += 1;
    $start++;
}
echo 'NUMBERS DESCREASING:';
echo '<br>';

// $start:11;
while($start > 0){
    //$start = $start - 1;
    //$start -= 1;
    $start--;
    echo 'number: ' . $start;
    echo '<br>';
}

$i = 0;
while($i <= 14){
    echo '<h2>'. $titles[$i] .'</h2>';
    $i++;
}

$i = 0;
do{
    echo $i;
    echo '<br>';
    $i++;
}while($i <= 10);

//for(segment1;segment2;segment3)
// segment1 - create iterator(action is done 1 time before execution of cycle
// segment2 - condition (if iteration should be made)
// segment3 - interator update (action is done after each iteration)
echo 'FOR increasing:';
echo '<br>';
for($y = 0; $y <= 10; $y++){
    echo $y;
    echo '<br>';
}
echo 'FOR decreasing:';
echo '<br>';
for($z = 10; $z < 0; $z--){
    echo $z;
    echo '<br>';
}
//FOR
echo 'FOR array:';
echo '<br>';
for($z = 0;$z <= 14;$z++){
    $title = $titles[$z];
    echo $title.',';
    echo '<br>';
}
echo '<br>';
echo 'FOReach:';
echo '<br>';
//FOREACH
foreach($titles as $title){
    $title = $title . ',';
    echo $title;
    echo '<br>';
}
echo '<pre>';
var_dump($titles);
for($z = 0;$z <= 14;$z++){
    $titles[$z] = $titles[$z] . '!';
}
echo '<pre>';
var_dump($titles);
foreach($titles as $keyOfElement => $valueOfElement){
    echo $keyOfElement . ' ' . $valueOfElement . '<br>';
}
?>
