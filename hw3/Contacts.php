<?php 
        $namber="38(099)000 00 00";
        $adres="г.Днепр, ул. Глинки 2";
        $email="albina.ishchenko.1995@gmail.com";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My third home work</title>
    <meta name="description" content="We always add meta">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="header">
                    <ul class="nav navbar-dark bg-primary">
                        <li class="nav-item ">
                            <a class="nav-link color white-text" href="index.php">Lesson3</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link color white-text" href="Calculator_page.php">Calculator page</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link white-text active" href="Contacts.php">Contacts</a>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <h1 class="text-center">Contacts</h1>
                    <table border="0" width="100%" cellpadding="5">
                        <tbody>
                        
                            <tr height="25">
                                <td width="25%">Телефоны:</td>
                                <td> <?= "$namber"?></td>
                            </tr>
                            <tr height="25">
                                <td>Адрес:</td>
                                <td><?= "$adres"?></td>
                            </tr>
                            <tr height="25">
                                <td>e-mail:</td>
                                <td><?= "$email"?></td>
                            </tr>
                        </tbody>
                    </table>
                    <h2 class="text-center">Feedback form:</h2>
                    <form action="Contacts.php" method="GET">
                        <div class="mb-3">
                            <label for="exampleInputEmail1" class="form-label">Email address</label>
                            <input type="email" name="email" class="form-control" id="exampleInputEmail1"
                                aria-describedby="emailHelp" value="Email address">

                            <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div>
                        </div>
                        <div class="mb-3">
                            <label for="exampleInputName" class="form-label">Name</label>
                            <input type="text" name="Name" class="form-control" id="exampleInputName"
                                aria-describedby="emailName" value="Name">
                        </div>
                        <div class="mb-3">
                            <label for="exampleInputQuestion" class="form-label">Question</label>
                            <input type="Question" class="form-control" id="exampleInputQuestion" value="Question">
                        </div>
                        <div class="mb-3 form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1" >Check me out</label>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>

                    </form>
                </div>
                <div class="footer">
                    <p class="text-center">© 2019 Lesson3</p>
                </div>
            </div>
        </div>

    </div>

</body>

</html>