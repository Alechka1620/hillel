<?php 
        $grivna=1500;
        $doll=25.2323;
        $evro=30.34224;
                ?>
<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My third home work</title>
    <meta name="description" content="We always add meta">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="header">
                    <ul class="nav navbar-dark bg-primary">
                        <li class="nav-item ">
                            <a class="nav-link color white-text" href="index.php">Lesson3</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link color white-text" href="Calculator_page.php">Calculator page</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link white-text" href="Contacts.php">Contacts</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="body"></div>
        <h1 class="text-center">Calculator page</h1>
        

        <?= "Пример $grivna грн - это ".round($grivna/$doll,2)."$ или " .round($grivna/$evro,2) . "Евро";
        /*В результате изменения переменной содержащей значение гривны, должны меняться и отображаемые перерасчеты*/
 ?>
        <form>
            <div class="mb-3">
                <label class="text-center">Валюта ввода </label>
                <select class="form-select">
                    <option selected>Валюта</option>
                    <option value="1">Гривна</option>
                    <option value="2">Доллар</option>
                    <option value="3">Евро</option>
                </select>
            </div>
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Количество</label>
                <input type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
            </div>
            <div class="mb-3">
                <label class="text-center">Валюта конвертации</label>
                <select class="form-select">
                    <option selected>Валюта</option>
                    <option value="1">Гривна</option>
                    <option value="2">Доллар</option>
                    <option value="3">Евро</option>
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Конвертировать</button>
        </form>
        <div class="footer">
            <p class="text-center">© 2019 Lesson3</p>
        </div>

    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous">
    </script>
</body>

</html>