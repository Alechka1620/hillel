<?php 
        $x="Евро";
        $y="Доллар";
        $doll="25,2323";
        $evro="30,34224";
?>
<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My third home work</title>
    <meta name="description" content="We always add meta">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="header">
                    <ul class="nav navbar-dark bg-primary">
                        <li class="nav-item ">
                            <a class="nav-link color white-text" href="index.php">Lesson3</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link color white-text" href="Calculator_page.php">Calculator page</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link white-text" href="Contacts.php">Contacts</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="body">
            <h1 class="text-center">Информация о курсе Евро и Доллара</h1>
            <p>Наиболее популярные курсы валют, интересующие украинцев практически ежедневно, – это курсы доллара и евро
                к
                гривне. К сожалению, гривна в последнее время резко упал по отношению к этим валютам. По мнению многих
                аналитиков, это стало следствием внутренней и внешней политики Украины. В настоящее время падение гривна
                продолжается и совершенно неясно, на каком уровне оно остановится.</p>
            <p>Большинство граждан ежедневно отслеживает курс доллара на сегодня и курс евро на сегодня, в зависимости
                от
                того, какая из этих валют кажется им более важной. Но мало кто знает, как именно устанавливается этот
                курс.
            </p>
            <p>Дело в том, что курс доллара на сегодня устанавливается Банком Украины днем раньше. Фактически, каждый
                день
                устанавливается курс доллара на завтра. Официальный курс доллара США по отношению к гривне
                рассчитывается и
                устанавливается Банком Украины на основе котировок межбанковского внутреннего валютного рынка. С 15
                апреля
                2003 года для установления официального курса доллара используется средневзвешенное значение курса
                доллара
                США на торгах Единой торговой сессии межбанковских валютных бирж со сроком расчетов "завтра",
                сложившееся по
                состоянию на 11 часов 30 минут дня торгов </p>
            <p>Таким образом, курс доллара на завтра устанавливается Банком Украины ежедневно (по рабочим дням) в 11
                часов
                30 минут торгового дня по среднему значению его стоимости на биржевых торгах.</p>
            <p>Аналогичным образом, курс евро на сегодня был установлен Банком Украины еще вчера, но по другому
                алгоритму.
                Курс евро на завтра определяется не напрямую, по среднему значению на торгах в определенное время, а на
                основе уже установленного курса доллара на завтра, с учетом котировки евро к доллару США на
                международных
                валютных рынках и на межбанковском внутреннем валютном рынке.</p>
        </div>
        <h2 class="text-center"> Курс валют</h2>
        <table>
            <tr>
                <td>Валюта</td>
                <td>Курс</td>
            </tr>
            <tr>
                <td><?= $x; ?></td>
                <td><?= $evro; ?></td>
            </tr>
            <tr>
                <td><?= $y; ?></td>
                <td><?= $doll; ?></td>
            </tr>

        </table>
        <div class="footer">
            <p class="text-center">© 2019 Lesson3</p>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous">
    </script>
</body>

</html>