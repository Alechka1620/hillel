<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My second home work</title>
    <meta name="description" content="We always add meta">
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="navigation">
        <div class="navigation-menu">
            <a class="navigation-elem" href="/"><img class="navigation-logo" src="assets/header_logo.svg" alt="Hillel student"></a>
            <a class="navigation-elem" href="#">Blog</a>
            <a class="navigation-elem" href="#">Shop</a>
            <a class="navigation-elem" href="#">Contacts</a>
            <a class="navigation-elem" href="#">About me</a>
        </div>
        <div class="navigation-info">
            <p class="navigation-info-text">Our website works till the latest visitor!</p>
        </div>
    </div>
    <div class="content">
        <div class="content-intro">
            <h1>Hey I am a student learning HTML and CSS in Hillel.</h1>
            <p class="content-wide-text">Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum, quos numquam fugit natus laborum ea minus omnis, esse doloribus, error totam! Aut commodi, praesentium nisi et fugit libero beatae illo?</p>
        </div>
        <div class="content-float-skills">
            <h2>I can do floating:</h2>
            <div class="box">100px</div>
            <div class="box" id="box2">200px</div>
            <div class="box"></div>
            <div class="box" id="box4">300px</div>
            <div class="box"></div>
            <div class="box small-box">50px</div>
            <div class="box small-box"></div>
            <div class="box small-box"></div>
            <div class="box small-box"></div>
            <div class="box"></div>
            <div class="content-add-text">
                <p class="content-wide-text">Yes I can do like so!</p>
            </div>
        </div>
    </div>
    <div class="footer">
        <p class="footer-text">© Hillel Student</p>
    </div>
</body>
</html>