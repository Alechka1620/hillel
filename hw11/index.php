<?php

require_once $_SERVER['DOCUMENT_ROOT']."/hw11/PDO.php";
//require_once $_SERVER['DOCUMENT_ROOT']."/hw11/create_db.php";
//require_once $_SERVER['DOCUMENT_ROOT']."/hw11/seeder_db.php";
require_once $_SERVER['DOCUMENT_ROOT']."/hw11/classes/Person.php";
require_once $_SERVER['DOCUMENT_ROOT']."/hw11/classes/student.php";
require_once $_SERVER['DOCUMENT_ROOT']."/hw11/classes/teacher.php";
require_once $_SERVER['DOCUMENT_ROOT']."/hw11/classes/admin.php" ;

try{
    $sql = "SELECT * FROM members";
    $responsObject = $db->query($sql);
    $people=$responsObject->fetchAll();
}catch (Exception $a){
    die('Error getting members! <br>'.$a->getMessage());

};
$peopleObjects = [];
foreach($people as $men){
    switch($men['role']){
        case 'student': 
            $peopleObjects[]= new Student($men['full_name'], 
            $men['phone'], $men['email'], 
            $men['role'], $men['averange_mark']);
        break;
        case 'teacher':
            $peopleObjects[]= new Teacher($men['full_name'], 
            $men['phone'], $men['email'], 
            $men['role'], $men['subject']);
        break;
        case 'admin': 
            $peopleObjects[]= new Admin($men['full_name'], 
            $men['phone'], $men['email'], 
            $men['role'], $men['working_day']);
        break;

    }
}


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Homework</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <div class="row">
            <?php foreach ($peopleObjects as $peopleObject): ?>
                <div class="col-4">
                    <div class="card">
                        <div class="card-body">
                            <?= $peopleObject->getVisitCard() ?>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</tbody>
        
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous">
    </script>

</body>
  
</html>