<?php
try{
    $db = new PDO('mysql:host=localhost;dbname=members', 'root' , 'root');
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->exec('SET NAMES "utf8"');
}catch(Exception $exception){
    echo 'Database is on maintance';
    echo $exception->getMessage();
    die();
}
?>