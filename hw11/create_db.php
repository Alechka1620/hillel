<?php
require_once $_SERVER['DOCUMENT_ROOT']."/hw11/PDO.php";

try{
    $sql = 'CREATE TABLE members (
        id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        full_name VARCHAR(255),
        phone VARCHAR(255),
        email VARCHAR(255),
        role VARCHAR(255),
        averange_mark FLOAT,
        subject VARCHAR(255),
        working_day VARCHAR(255)
        ) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB;';
    $db->exec($sql);
}catch(Exception $excptn){
    echo 'Error creating TABLE: notes<br>';
    echo $excptn->getMessage();
    die();
}
echo 'TABLE members created succesfully!';
die();
?>