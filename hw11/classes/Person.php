<?php
class Person {
    public $full_name;
    public $phone;
    public $email;
    public $role;
    function __construct ($full_name, $phone, $email, $role ){
        $this->full_name=$full_name;
        $this->phone=$phone;
        $this->email=$email;
        $this->role=$role;
    }
    public function getVisitCard(){
        return  $this->full_name .' '. $this->phone .' '. $this->email .' ' . $this->role;
    }
}
?>
