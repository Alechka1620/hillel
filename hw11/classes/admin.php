<?php

class Admin extends Person{
    public $working_day=0;
    public function __construct($full_name, $phone, $email, $role, $working_day ){
        parent::__construct($full_name, $phone, $email, $role);
        $this->working_day=$working_day;
    }
    public function getVisitCard(){
        return  parent::getVisitCard() .' working day - '. $this->working_day ;
    }
}
?>