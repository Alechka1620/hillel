<?php

class Teacher extends Person{
    public $subject=0;
    public function __construct($full_name, $phone, $email, $role, $subject){
        parent::__construct($full_name, $phone, $email, $role);
        $this->subject=$subject;
    }
    public function getVisitCard(){
        return  parent::getVisitCard() .'subject-' . $this->subject ;
    }
}
?>