
<?php

class Student extends Person{
    public $averange_mark=0;
    public function __construct($full_name, $phone, $email, $role, $averange_mark){
        parent::__construct($full_name, $phone, $email, $role);
        $this->averange_mark=$averange_mark;
    }
    public function getVisitCard(){
        return  parent::getVisitCard() .'averange mark-' . $this->averange_mark ;
    }
}
?>
