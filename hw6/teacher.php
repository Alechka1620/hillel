<?php
$persons=[
    ['name' => 'John Doe' , 'group' => 'student', 'email' => 'john@gmail.com', 'phone' =>'123-213-12'],
    ['name' => 'Anna Both' , 'group' => 'teachers', 'email' => 'both@gmail.com', 'phone' =>'333-213-12'],
    ['name' => 'Julia Doe' , 'group' => 'admin', 'email' => 'julia@gmail.com', 'phone' =>'223-213-12'],
    ['name' => 'John Benon' , 'group' => 'student', 'email' => 'benon@gmail.com', 'phone' =>'553-213-12'],
    ['name' => 'Den Bar' , 'group' => 'teachers', 'email' => 'bard@gmail.com', 'phone' =>'663-213-12'],
    ['name' => 'Petro Troe' , 'group' => 'admin', 'email' => 'troe@gmail.com', 'phone' =>'777-213-12'],
    ['name' => 'Santa Das' , 'group' => 'student', 'email' => 'santa@gmail.com', 'phone' =>'888-213-12'],
    ['name' => 'Sinti Korn' , 'group' => 'teachers', 'email' => 'sinti@gmail.com', 'phone' =>'111-213-12'],
    ['name' => 'Vivien Soup' , 'group' => 'student', 'email' => 'vivien@gmail.com', 'phone' =>'234-213-12'],
    ['name' => 'Asti Krou' , 'group' => 'admin', 'email' => 'asti@gmail.com', 'phone' =>'652-213-12']
];
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Homework </title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="header">
                    <ul class="nav navbar-dark ">
                        <li class="nav-item ">
                            <a class="nav-link color white-text" href="index.php">Главная</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link color white-text" href="student.php">Студенты</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link white-text active" href="teacher.php">Преподаватели</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link white-text active" href="admin.php">Администраторы</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <table class="table col-sm-12">
                <thead>
                    <tr>
                        <th scope="col">name</th>
                        <th scope="col">group</th>
                        <th scope="col">email</th>
                        <th scope="col">phone</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($persons as $teachers) :?>
                        <?php if($teachers['group'] === 'teachers') :?>
                            <tr>
                                <th><?= $teachers['name']?></th>
                                <td><?= $teachers['group']?></td>
                                <td><?=$teachers['email']?></td>
                                <th><?=$teachers['phone']?></th>
                            </tr>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous">
    </script>
</body>

</html>