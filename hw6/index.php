<?php

$persons=[
    ['name' => 'John Doe' , 'group' => 'student', 'email' => 'john@gmail.com', 'phone' =>'123-213-12'],
    ['name' => 'Anna Both' , 'group' => 'teachers', 'email' => 'both@gmail.com', 'phone' =>'333-213-12'],
    ['name' => 'Julia Doe' , 'group' => 'admin', 'email' => 'julia@gmail.com', 'phone' =>'223-213-12'],
    ['name' => 'John Benon' , 'group' => 'student', 'email' => 'benon@gmail.com', 'phone' =>'553-213-12'],
    ['name' => 'Den Bar' , 'group' => 'teachers', 'email' => 'bard@gmail.com', 'phone' =>'663-213-12'],
    ['name' => 'Petro Troe' , 'group' => 'admin', 'email' => 'troe@gmail.com', 'phone' =>'777-213-12'],
    ['name' => 'Santa Das' , 'group' => 'student', 'email' => 'santa@gmail.com', 'phone' =>'888-213-12'],
    ['name' => 'Sinti Korn' , 'group' => 'teachers', 'email' => 'sinti@gmail.com', 'phone' =>'111-213-12'],
    ['name' => 'Vivien Soup' , 'group' => 'student', 'email' => 'vivien@gmail.com', 'phone' =>'234-213-12'],
    ['name' => 'Asti Krou' , 'group' => 'admin', 'email' => 'asti@gmail.com', 'phone' =>'652-213-12']
];
if (isset($_POST['age']) && $_POST['age']>=18){
    $persons[]=['name' => $_POST['name'] , 'group' => $_POST['group'], 'email' => $_POST['email'], 'phone' =>$_POST['namber']];
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> Homework </title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="header">
                    <ul class="nav navbar-dark ">
                        <li class="nav-item ">
                            <a class="nav-link color white-text" href="index.php">Главная</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link color white-text" href="student.php">Студенты</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link white-text active" href="teacher.php">Преподаватели</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link white-text active" href="admin.php">Администраторы</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <form action="index.php" method="POST">
                    <div class="mb-3">
                        <label for="exampleInputName" class="form-label">ФИО</label>
                        <input type="name" class="form-control" id="exampleInputName" aria-describedby="NameHelp"
                            name="name" value="">
                    </div>
                    <div>
                        <select class="form-select" aria-label="Default select example" name="group"
                            value="">
                            <option value="student">Студент</option>
                            <option value="admin">Администратор</option>
                            <option value="teachers">Преподаватель</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputAge" class="form-label">Возраст</label>
                        <input type="text" class="form-control" id="exampleInputAge" aria-describedby="AgeHelp"
                            name="age" value="">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputEmail1" class="form-label">Email address</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp"
                            name="email" value="">
                    </div>
                    <div class="mb-3">
                        <label for="exampleInputPhone" class="form-label">Телефон</label>
                        <input type="text" class="form-control" id="exampleInputPhone" aria-describedby="PhoneHelp"
                            name="namber" value="">
                    </div>
                    <button type="submit" class="btn btn-primary">Отправить</button>

                    <?php if (isset($_POST['age']) && $_POST['age']<18):?>
                        <div>
                            <p> Извините, Вы слишком молоды </p>
                        </div>
                    <?php elseif(isset($_POST['age']) && $_POST['age']>=18):?>
                        <?php if ($_POST['group'] =="student" ):?>
                            <div>
                                <p> Вы успешно зарегистрированы и добавлены в список Студентов </p>
                            </div>
                        <?php elseif($_POST['group'] =="admin" ):?>
                            <div>
                                <p> Вы успешно зарегистрированы и добавлены в список Администраторов </p>
                            </div>
                        <?php elseif($_POST['group'] =="teachers" ):?>
                            <div>
                                <p> Вы успешно зарегистрированы и добавлены в список Преподавателей </p>
                            </div>
                
                        <?php endif; ?>
                    <?php endif; ?>

                </form>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <table class="table col-sm-12">
                <thead>
                    <tr>
                        <th scope="col">name</th>
                        <th scope="col">group</th>
                        <th scope="col">email</th>
                        <th scope="col">phone</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($persons as $person): ?>
                    <tr>
                        <th><?= $person['name']?></th>
                        <td><?= $person['group']?></td>
                        <td><?=$person['email']?></td>
                        <th><?=$person['phone']?></th>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous">
    </script>
</body>

</html>