<?php
class Userr
{
    public $id;
    public $password;

    public function getUserData()
    {
        return $this->id . ' ' . $this->password;
    }

    public function __construct($id, $password)
    {
        if (is_int($id) && strlen($password) >= 8) {
            $this->id = $id;
            $this->password = $password;
        } else {
            throw new Exception;
        }
    }
}

try {
    $user1 = new Userr(66, 'passwordd');
    echo $user1->getUserData();
} catch (Exception $e) {
    echo "Исключение было создано на строке: " . $e->getLine() . " Файла, в котором исключение было создано:" . $e->getFile();
}
