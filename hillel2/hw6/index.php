<?php 
trait Trait1 
{
    public function method()
    {
        return '1';
    }
}
trait Trait2
{
    public function method()
    {
        return '2';
    }
}
trait Trait3
{
    public function method()
    {
        return '3';
    }
}

class Test {

    public $number;

    use Trait1, Trait2, Trait3 {
        Trait1::method insteadof Trait2;
        Trait1::method insteadof Trait3;
        Trait2::method as method1;
        Trait3::method as method2;

    }
    public function getSum() 
    {
        return $this->method() + $this->method1() + $this->method2();
    }
}

$test = new Test;
echo $test->getSum();

