<?php
abstract class Model
{
    public static function find($id)
    {
        return 'SELECT * FROM user WHERE id =' . $id; 

    }
    
    abstract function create();

    abstract function update();

    abstract function delete();

}

final class User extends Model
{
    public $id;
    public $name;
    public $email;

    
    public function create()
    {
        return "INSERT INTO user (id, name, email) VALUES ($this->id, $this->name, $this->email)";
    }

    public function update()
    {
        return "UPDATE user SET name = $this->id , email = $this->email WHERE id = $this->id";
    }

    public function delete()
    {
        return "DELETE user WHERE id = $this->id";
    }

    public function save()
    { 
        if ($this->id == NULL) {
            return $this->create();
        } else {
            return $this->update();
        }
    }

}

$user = User::find(1);
var_dump($user); // SELECT * FROM user WHERE id = :id

$user = new User;
$user->name = 'John';
$result = $user->save();
var_dump($result); // UPDATE user SET name = :name, email = 'email' WHERE id = :id 

$result = $user->delete();
var_dump($result); // DELETE user WHERE id = :id

$user = new User;
$user->name = 'John';
$user->email = 'some@gmail.com';
$result = $user->save();
var_dump($result); // INSERT INTO user (id, name, email) VALUES (:id, :name, :email)