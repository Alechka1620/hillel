<?php

declare(strict_types=1);
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);



class Logger
{

    private Format $format;
    private Deliver $deliver;

    public function __construct(Format $format, Deliver $deliver)
    {
        $this->format = $format;
        $this->deliver = $deliver;
    }

    public function log($string)
    {
        $this->deliver->deliver($this->format->format($string));
    }
}

interface Format
{
    public function format($string);
}

class FormatRaw implements Format
{
    public function format($string)
    {
        return $string;
    }
}

class FormatWithDate implements Format
{
    public function format($string)
    {
        return date('Y-m-d H:i:s') . $string;
    }
}

class FormatWithDateAndDetails implements Format
{
    public function format($string)
    {
        return date('Y-m-d H:i:s') . $string . ' - With some details';
    }
}

interface Deliver
{
    public function deliver($format);
}

class DeliverByEmail implements Deliver
{
    public function deliver($format)
    {
        echo "Вывод формата ({$format}) по имейл";
    }
}

class DeliverBySms implements Deliver
{
    public function deliver($format)
    {
        echo "Вывод формата ({$format}) в смс";
    }
}

class DeliverToConsole implements Deliver
{
    public function deliver($format)
    {
        echo "Вывод формата ({$format}) в консоль";
    }
}

$format = new FormatRaw();
$deliver = new DeliverByEmail();
$logger = new Logger($format, $deliver);
$logger->log('Emergency error! Please fix me!');
