<?php

namespace Core;

class Router
{
    protected $routes = [];
    protected $params = [];

    // public function dispatch(string $url)
    // {
    // }
    protected $convertTypes = [
        "d" => "int",
        "s" => "string"
    ];

    protected $controllerNamespace = "App\Controllers\\";

    public function add(string $route, array $params = [])
    {
        //       dd($route);
        $route = preg_replace("/\//", "\\/", $route);
        $route = preg_replace("/\{([a-z]+)\}/", "(?P<\1>[a-z-]+)", $route);
        $route = preg_replace("/\{([a-z]+):([^\}]+)\}/", "(?P<\1>\2)", $route);
        $route = "/^" . $route . "$/i";
        $this->routes[$route] = $params;
        return $this;
    }


    public function getRoutes(): array
    {
        return $this->routes;
    }

    public function getParas(): array
    {
        return $this->params;
    }

    public function match(string $url): bool
    {
        // dd($url, $this->routes);
        foreach ($this->routes as $route => $params) {
            if (preg_match($route, $url, $matches)) {
                preg_match_all("/\(\?P<[\w]+>\\\\([\w\+]+)\)", $route, $types);

                $step = 0;

                foreach ($matches as $key => $match) {
                    if (is_string($key)) {
                        $type = trim($types[1][$step], "+");
                        settype($match, $this->convertTypes[$type]);
                        $params[$key] = $match;
                        $step++;
                    }
                }

                $this->params = $params;
                return true;
            }
        }

        return false;
    }

    public function dispatch(string $url = "")
    {

        $url = $this->removeQueryStringVariables($url);

        if ($this->match($url)) {
            $controller = $this->params['controller'];
            $controller = $this->getNamespace() . $this->convertToStudlyCaps($controller);

            if (class_exists($controller)) {
                $controllerObject = new $controller($this->params);

                $action = $this->params['action'];
                $action = $this->convertToCamelCase($action);

                if (preg_match("/action$/i", $action) == 0) {
                    call_user_func([$controllerObject, $action]);
                } else {
                    $mist = "Metod {$action} in controller {$controller} cannot be directly called ";
                    throw new \Exception($mist);
                }
            } else {
                throw new \Exception("Controller class {$controller} not found");
            }
        } else {
            throw new \Exception("No route matched", 404);
        }
    }

    protected function convertToStudlyCaps($string)
    {
        return str_replace(' ', '', ucwords(str_replace('-', ' ', $string)));
    }

    protected function convertToCamelCase($string)
    {
        return lcfirst($this->convertToStudlyCaps($string));
    }

    protected function removeQueryStringVariables($url)
    {
        if ($url != "") {
            $parts = explode("&", $url, 2);
            if (strpos($parts[0], "=")) {
                $url = str_replace('url=', '',  $parts[0]);
            } else {
                $url = "";
            }
        }
        return $url;
    }

    protected function getNamespace(): string
    {
        $namespace = "App\Controllers\\";

        if (array_key_exists("namespace", $this->params)) {
            $namespace .= $this->params["namespace"] . "\\";
        }

        return $namespace;
    }
}
