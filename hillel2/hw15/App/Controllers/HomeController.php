<?php

namespace App\Controllers;

class HomeController
{
    public function index()
    {
        $result = [
            'controller' => 'Home',
            'action' => 'index',
            'params' => []
        ];
        dd($result);
    }

    public function show(int $id)
    {
        $result = [
            'controller' => 'Home',
            'action' => 'show',
            'params' => [
                'id' => $id
            ]
        ];
        dd($result);
    }
}
