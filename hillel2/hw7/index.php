<?php
class User
{
    private $name;
    private $age;
    private $email;

    private function setName($name)
    {
        return $this->name = $name;
    }

    private function setAge($age)
    {
        return $this->age = $age;
    }

    public function  __call($name, $arguments)
    {
        if (method_exists($this, $name)) {
            return call_user_func_array(array($this, $name), $arguments);
        } else {
            echo 'Вы хотели вызвать $newUser->' . $name . ', но его не существует';
        }
    }

    public function getAll()
    {
        return $this->name  . ' ' . $this->age  . ' ' . $this->email;
    }
}

$newUser = new User();
echo $newUser->setEmail('hvknkbkbk');
echo '<pre>';
echo $newUser->setName('Albina');
echo '<pre>';
echo $newUser->setAge(18);
echo '<pre>';
echo $newUser->getAll();
