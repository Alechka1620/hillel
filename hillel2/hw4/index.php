<!-- Отличие абстрактного класса от интерфейса:
1. Абстрактный класс может иметь методы и данные. Методы могут как иметь, так и не иметь реализацию. Наследник может реализовать данный метод, а может и не реализовывать, но если метод объявлен как абстрактный, то наследник обязан иметь реализацию этого метода. Интерфейс может иметь только методы без реализации.
2. Наследование: можно наследовать только от одного абстрактного класса, не поддерживаем множественное наследование. Можно реализовать несколько интерфейсов. Интерфейс поддерживает множественное наследование
3. Абстрактный класс наследуется(etxends), а интерфейс реализуется (implements). 
4. Интерфейс не может иметь модификаторов доступа - все методы по умолчанию публичные, абстракный класс может иметь модификаторы доступа-->

<?php
// interface
interface Interface 
{
    public function get($key);
    public function set($key,$value);
}

class Memcache implements Interface 
{
    public function get($key)
	{
        // method body
    }

    public function set($key,$value)
	{
	// method body
    }
}
// Abstract class
abstract class car 
{
	abstract public function modelType(); 
	public function wheelCount() 
	{
		echo "I have four wheels";
	}
}
class HondaCity extends car
{
	public function modelType()
	{
		echo "HondaCity";
	}
}

$data = new HondaCity;
$data->modelType();
$data->wheelCount();
