<?php
class Color 
{
    private $red;
    private $green;
    private $blue;

    private $errorMessage = "Error Color"; 

    public function __construct($red, $green, $blue) 
    {
        try {
            $this -> setGreen($green);
            $this -> setRed($red);
            $this -> setBlue($blue);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    public function getRed() 
    {
        return $this->red;
    }

    public function getGreen() 
    {
        return $this->green;
    }

    public function getBlue() 
    {
        return $this->blue;
    }

    public function range($color)
    {
        return $color >= 0 && $color <= 255;
    }

    private function setRed($red) 
    {
        if($this->range($red)) {
            $this->red = $red;
        } else {
            throw new Exception($this->errorMessage);
        }
    }
    
    private function setGreen($green) 
    {
        if($this->range($green)) {
            $this->green = $green;
        } else {
            throw new Exception($this->errorMessage);
        }
    }

    private function setBlue($blue) 
    {
        if($this->range($blue)) {
            $this->blue = $blue;
        } else {
            throw new Exception($this->errorMessage);
        }
    }
    public function equals($color): bool
    {
        return $$color == $this;
    }
    
    public function mix(Color $color)
    {
        try{
            $color->setRed(($color->getRed() + $this->getRed())/2);
            $color->setGreen(($color->getGreen() + $this->getGreen())/2);
            $color->setBlue(($color->getBlue() + $this->getBlue())/2);
            return $color;
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }
    
    public function random()
    {
        return new Color(rand(0,255), rand(0,255), rand(0,255));
    }
}
$color = new Color(200, 200, 200);
$mixedColor = $color->mix(new Color(100, 100, 100));
echo $mixedColor->getRed(); // 150
echo $mixedColor->getGreen(); // 150
echo $mixedColor->getBlue(); // 150