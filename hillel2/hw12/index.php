<?php

declare(strict_types=1);
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

interface Car
{
    public function modelCar();
    public function priceCar();
}

class EconomCarType implements Car
{
    public function modelCar()
    {
        return "Model Econom";
    }
    public function priceCar()
    {
        return "Price Econom";
    }
}

class StandardCarType implements Car
{
    public function modelCar()
    {
        return "Model Standard";
    }
    public function priceCar()
    {
        return "Price Standard";
    }
}

class LuxuryCarType implements Car
{
    public function modelCar()
    {
        return "Model Luxury";
    }
    public function priceCar()
    {
        return "Price Luxury";
    }
}

abstract class TaxiSystem
{
    abstract public function delivery(): Car;
    public function someOperation(): string
    {
        $model = $this->delivery()->modelCar();
        $price = $this->delivery()->priceCar();
        return $model . ' ' .  $price;
    }
}

class EconomCar extends TaxiSystem
{
    public function delivery(): Car
    {
        return new EconomCarType();
    }
}

class LuxuryCar extends TaxiSystem
{
    public function delivery(): Car
    {
        return new LuxuryCarType();
    }
}

class StandardCar extends TaxiSystem
{
    public function delivery(): Car
    {
        return new StandardCarType();
    }
}

$zgrSH = new EconomCar();
var_dump($zgrSH->someOperation());
// var_dump($zgrSH->delivery()->modelCar());