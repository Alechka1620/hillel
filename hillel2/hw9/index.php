<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/hillel2/hw9/notes/PDO.php";

// var_dump($tableExists);
// var_dump(count($_POST), empty($tableExists));

if (!empty($_POST) && empty($tableExists)) {
    try {
        $sql = 'CREATE TABLE user (
            id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
            name VARCHAR(255),
            surname VARCHAR(255),
            age INT,
            email VARCHAR(255),
            phone VARCHAR(255)
            ) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB;';
        $db->exec($sql);
    } catch (Exception $excptn) {
        echo 'Error creating TABLE: user<br>';
        echo $excptn->getMessage();
        die();
    }
    echo 'TABLE user created succesfully!';
    die();
} elseif (empty($tableExists)) {
?>

    <form action="" method='POST'>
        <input type="hidden" name="createTable" value="createTable">
        <button type="submit" class="btn btn-primary">Create table Users</button>
    </form>
<?php

} elseif (!empty($tableExists)) {
    echo 'Table user created';
}
// var_dump($tableExists);
// exit;



if (!empty($tableExists)) {
    require_once $_SERVER['DOCUMENT_ROOT'] . "/hillel2/hw9/notes/form1.php";
    try {
        if (!empty($_GET)) {
            $data = array($_GET['name'], $_GET['surname'], $_GET['age'], $_GET['email'], $_GET['phone']);
            $query = $db->prepare("INSERT INTO user (name, surname, age, email, phone) VALUES (?, ?, ?, ?, ?);");
            $query->execute($data);
            echo 'Record created in table!';
        }
        die();
    } catch (Exception $excptn) {
        echo 'Error creating new Record <br>';
        echo $excptn->getMessage();
        die();
    }
} else {
    echo 'Creat NEW Table';
}


?>