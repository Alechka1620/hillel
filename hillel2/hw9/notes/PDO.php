<?php
try {
    $db = new PDO('mysql:host=localhost;dbname=users;charset=utf8', 'root', 'root');
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->exec('SET NAMES "utf8"');
    $query = $db->prepare("Show tables like 'user';");
    $query->execute([]);
    $tableExists = $query->fetchAll();
} catch (Exception $exception) {
    echo 'Database is on maintance';
    echo $exception->getMessage();
    die();
}
