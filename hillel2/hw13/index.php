<?php

declare(strict_types=1);
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

interface SlowPaymentInterface
{
    public function getSlowPaymentText();
}

interface FastPaymentInterface
{
    public function getFastPaymentText();
}

class SlowPayment implements SlowPaymentInterface
{
    public function getSlowPaymentText(): string
    {
        return 'Slow payment';
    }
}

class FastPayment implements FastPaymentInterface
{
    public function getFastPaymentText(): string
    {
        return 'Fast payment';
    }
}
interface PaymentInterface
{
    public function slowPaymentGet(): SlowPaymentInterface;
    public function fastPaymentGet(): FastPaymentInterface;
}

class PaymentSystemType implements PaymentInterface
{
    public function slowPaymentGet(): SlowPaymentInterface
    {
        return new SlowPayment();
    }
    public function fastPaymentGet(): FastPaymentInterface
    {
        return new FastPayment();
    }
}

abstract class AbstractPaymentSystem
{
    abstract public function payment(): PaymentInterface;
    public function someOperation(): string
    {
        $cash = $this->payment()->slowPaymentGet()->getSlowPaymentText();
        $remittance = $this->payment()->fastPaymentGet()->getFastPaymentText();
        return $cash . ' ' .  $remittance;
    }
}

class PaymentSystem1 extends AbstractPaymentSystem
{
    public function payment(): PaymentInterface
    {
        return new PaymentSystemType();
    }
}
$factory = new PaymentSystem1();
var_dump($factory->someOperation());
