<?php
class Worker {
    public $name;
    public $age;
    public $salary;
}

$person1=new Worker();
$person1->name='John';
$person1->age='25';
$person1->salary='1000';

$person2=new Worker();
$person2->name='Vasya';
$person2->age='26';
$person2->salary='2000';

echo $person1->age + $person2->age;
echo '<pre>';
echo $person1->salary + $person2->salary;
?>