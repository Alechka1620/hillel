<?php 
    $auto = ['Toyota Corolla', 'Range Rover', 'Mercedes-Benz', 'BMW', 'Honda Accord', 'Dodge Aries', 'Plymouth Reliant', 'Dodge Caravan', 'Ford Taurus', 'Range Rover'];
    $yeas = [1990, 1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999];
    $tour1 = [
        'title' => 'Weekend in Istanbul' , 
        'price' => 456 ,
        'country' => 'Турция',
        'date' => '30.08'
    ];
    $tour2 = [
        'title' => 'Жаркая турецкая ночь...' , 
        'price' => 988 ,
        'country' => 'Турция',
        'date' => '30.09'
    ];
    $tour3 = [
        'title' => 'Незабываемые выходные' , 
        'price' => 3099 ,
        'country' => 'Доминикана',
        'date' => '13.07'
    ];
    $tour4 = [
        'title' => 'Райское наслаждение' , 
        'price' => 2676 ,
        'country' => 'Доминикана',
        'date' => '04.07'
    ];
    $tour5 = [
        'title' => 'Сказачная Каппадокия' , 
        'price' => 878 ,
        'country' => 'Турция',
        'date' => '09.08'
    ];
   
    $kurs = 30.55;
    
?>
<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>My fourse home work</title>
    <meta name="description" content="Array Basics">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
</head>

<body>
    <div class="container">
        <div class="row">
            <table class="table col-sm-12">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Auto</th>
                        <th scope="col">Years</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td><?php echo $auto[0]; ?></td>
                        <td><?php echo $yeas[0]; ?></td>
                    </tr>
                    <tr>
                        <th scope="row">2</th>
                        <td><?php echo $auto[1]; ?></td>
                        <td><?php echo $yeas[1]; ?></td>
                    </tr>
                    <tr>
                        <th scope="row">3</th>
                        <td><?php echo $auto[2]; ?></td>
                        <td><?php echo $yeas[2]; ?></td>
                    </tr>
                    <tr>
                        <th scope="row">4</th>
                        <td><?php echo $auto[3]; ?></td>
                        <td><?php echo $yeas[3]; ?></td>
                    </tr>
                    <tr>
                        <th scope="row">5</th>
                        <td><?php echo $auto[4]; ?></td>
                        <td><?php echo $yeas[4]; ?></td>
                    </tr>
                    <tr>
                        <th scope="row">6</th>
                        <td><?php echo $auto[5]; ?></td>
                        <td><?php echo $yeas[5]; ?></td>
                    </tr>
                    <tr>
                        <th scope="row">7</th>
                        <td><?php echo $auto[6]; ?></td>
                        <td><?php echo $yeas[6]; ?></td>
                    </tr>
                    <tr>
                        <th scope="row">8</th>
                        <td><?php echo $auto[7]; ?></td>
                        <td><?php echo $yeas[7]; ?></td>
                    </tr>
                    <tr>
                        <th scope="row">9</th>
                        <td><?php echo $auto[8]; ?></td>
                        <td><?php echo $yeas[8]; ?></td>
                    </tr>
                    <tr>
                        <th scope="row">10</th>
                        <td><?php echo $auto[9]; ?></td>
                        <td><?php echo $yeas[9]; ?></td>
                    </tr>
                </tbody>
            </table>
            <table class="table col-sm-12">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Название тура</th>
                        <th scope="col">Цена тура в usd</th>
                        <th scope="col">Страна</th>
                        <th scope="col">Дата</th>
                        <th scope="col">Цена в грн</th>
                    </tr>
                    <tr>
                        <th scope="row">1</th>
                        <td><?php echo $tour1['title']; ?></td>
                        <td><?php echo $tour1['price']; ?></td>
                        <td><?php echo $tour1['country']; ?></td>
                        <td><?php echo $tour1 ['date']; ?></td>
                        <td><?php echo $tour1 ['price']*$kurs; ?></td>
                    </tr>
                    <tr>
                        <th scope="row">2</th>
                        <td><?php echo $tour2['title']; ?></td>
                        <td><?php echo $tour2['price']; ?></td>
                        <td><?php echo $tour2['country']; ?></td>
                        <td><?php echo $tour2 ['date']; ?></td>
                        <td><?php echo $tour2 ['price']*$kurs; ?></td>
                    </tr>
                    <tr>
                        <th scope="row">3</th>
                        <td><?php echo $tour3['title']; ?></td>
                        <td><?php echo $tour3['price']; ?></td>
                        <td><?php echo $tour3['country']; ?></td>
                        <td><?php echo $tour3 ['date']; ?></td>
                        <td><?php echo $tour3 ['price']*$kurs; ?></td>
                    </tr>
                    <tr>
                        <th scope="row">4</th>
                        <td><?php echo $tour4['title']; ?></td>
                        <td><?php echo $tour4['price']; ?></td>
                        <td><?php echo $tour4['country']; ?></td>
                        <td><?php echo $tour4 ['date']; ?></td>
                        <td><?php echo $tour4 ['price']*$kurs; ?></td>
                    </tr>
                    <tr>
                        <th scope="row">5</th>
                        <td><?php echo $tour5['title']; ?></td>
                        <td><?php echo $tour5['price']; ?></td>
                        <td><?php echo $tour5['country']; ?></td>
                        <td><?php echo $tour5 ['date']; ?></td>
                        <td><?php echo $tour5 ['price']*$kurs; ?></td>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous">
    </script>
</body>

</html> ?>