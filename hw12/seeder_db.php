<?php
require_once $_SERVER['DOCUMENT_ROOT']."/hillel/hw12/PDO.php";
require_once $_SERVER['DOCUMENT_ROOT']."/hillel/hw12/classes/note.php";

$tovars=[
    [
        'title' => 'Samsung Galaxy S21',
        'price' => 555,
        'description' =>  'Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt iste quo obcaecati',
        'type' =>  'phone',
    ],
    [
        'title' => 'Samsung Galaxy Book',
        'price' => 666,
        'description' =>  'Lorem ipsum dolor sit, amet consectetur adipisicing elit. Corrupti ',
        'type' =>  'laptop',
    ],
    [
        'title' => 'Samsung Galaxy Watch',
        'price' => 777,
        'description' =>  'Lorem ipsum dolor sit amet consectetur adipisicing',
        'type' =>  'watch',
    ],
    [
        'title' => 'Apple iPhone 12 Pro Max',
        'price' => 888,
        'description' =>  'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Autem voluptas blanditiis',
        'type' =>  'phone',
    ],
    [
        'title' => 'ASUS ZenBook',
        'price' => 999,
        'description' =>  'Lorem, ipsum dolor sit amet ',
        'type' =>  'laptop',
    ],
    [
        'title' => 'Apple Watch',
        'price' => 444,
        'description' =>  'Lorem ipsum dolor sit amet consectetur, adipisicing elit. Nostrum aperiam qui ',
        'type' =>  'watch',
    ],
    [
        'title' => 'iPhone 11',
        'price' => 333,
        'description' =>  'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ut rerum eos quas unde',
        'type' =>  'phone',
    ],
];

try{
    foreach($tovars as $tovar){
        $note = new Note($tovar['title'], $tovar['price'], $tovar['description'], $tovar['type']);
        $note->save($db);
    }    
}catch(Exception $e){
    die("Error while adding Test data.<br>".$e->getMessage());
}
echo "DATABASE Table shop was filled with " .count($tovars). " entries";

