<?php
if(empty($_POST['title']) || empty($_POST['price'])|| empty($_POST['description'])|| empty($_POST['type'])){
    header('Location:/notes/create.php');
}
require_once $_SERVER['DOCUMENT_ROOT']."/PDO.php";
require_once $_SERVER['DOCUMENT_ROOT']."/classes/note.php";

$title = htmlspecialchars($_POST['title'], ENT_QUOTES, 'UTF-8');
$body = htmlspecialchars($_POST['price'], ENT_QUOTES, 'UTF-8');
$body = htmlspecialchars($_POST['description'], ENT_QUOTES, 'UTF-8');
$body = htmlspecialchars($_POST['type'], ENT_QUOTES, 'UTF-8');

$note = new Note($title, $price, $description, $type);
$note->save($db);
header('Location:/?notification=entry_saved');
