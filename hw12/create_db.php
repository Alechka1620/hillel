<?php
require_once $_SERVER['DOCUMENT_ROOT']."/hillel/hw12/PDO.php";

try{
    $sql = 'CREATE TABLE shop (
        id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
        title VARCHAR(255),
        price FLOAT,
        description VARCHAR(255),
        type VARCHAR(255)
        ) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB;';       
    $db->exec($sql);
}catch(Exception $excptn){
    echo 'Error creating TABLE: shop <br>';
    echo $excptn->getMessage();
    die();
}
echo 'TABLE shop created succesfully!';
die();
?>