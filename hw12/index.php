<?php

require_once $_SERVER['DOCUMENT_ROOT']."/hillel/hw12/PDO.php";
require_once $_SERVER['DOCUMENT_ROOT']."/hillel/hw12/classes/note.php";

$notesObjects = Note::all($db);

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Homework</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
</head>
<body>



<div class="container">
    <div class="row">
        <?php if(!empty($_GET['notification'])):?>
            <?php if($_GET['notification'] == 'entry_saved'):?>
                <div class="alert alert-success" role="alert">
                    Note has been saved succesfully!
                </div>
            <?php endif;?>
        <?php endif;?>
    </div>
    <div class="row">
        <?php foreach($notesObjects as $note):?>
            <?php $note->showTemplate('notes/list')?>
        <?php endforeach;?>
    </div>
</div>

</tbody>
        
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous">
    </script>

</body>
  
</html>