<?php
class Note{
    protected $id = 0;
    protected $title = "";
    protected $price = "";
    protected $description = "";
    protected $type = "";

    public function __construct($title, $price, $description, $id = null ){
        $this->id = $id;
        $this->title = $title;
        $this->price = $price;
        $this->description = $description;
        $this->type = $type;
    }

    static public function create($id, $db){
        $sql = "SELECT * FROM shop WHERE id = :id;";
        $statement = $db->prepare($sql);
        $statement->bindValue(':id', $id);
        $statement->execute();
        $notesArray = $statement->fetchAll();
        return new self($notesArray[0]['title'], $notesArray[0]['price'], $notesArray[0]['description'], $notesArray[0]['type']);
    }

    static public function all($db){
        try{
            $sql = "SELECT * FROM shop";
            $responseObject = $db->query($sql);
            $notesArray = $responseObject->fetchAll();
            $notesObjects = [];
            foreach($notesArray as $noteArr){
                $notesObjects[] = new Note($noteArr['title'], $noteArr['price'], $noteArr['description'], $noteArr['type']);
            }
            return $notesObjects;
        }catch(Exception $e){
            die('Error getting notes!<br>'.$e->getMessage());
        }
    }
    static public function delete($id, $db){
        try{
            $sql = "DELETE FROM shop WHERE id=:id";
            $statement = $db->prepare($sql);
            $statement->bindValue(':id', $id);
            $statement->execute();
        }catch(Exception $e){
            die('Error deleting note<br>'.$e->getMessage());
        }
    }
    public function save(PDO $db){
        try{
            
            $sql = "INSERT INTO shop SET 
            title=:title,
            price=:price,
            description=:description,
            type=:type ";
            $statement = $db->prepare($sql);
            $statement->bindValue(':title', $this->title);
            $statement->bindValue(':price', $this->price);
            $statement->bindValue(':description', $this->description);
            $statement->bindValue(':type', $this->type);
            $statement->execute();
            
        }catch(Exception $e){
            die('Error creating new note!<br>' . $e->getMessage());
        }
    }
    public function update(PDO $db){
        try{
            $sql = "UPDATE shop SET
                title=:title,
                price=:price,
                description=:description,
                type=:type
             WHERE id = :id";
            $statement = $db->prepare($sql);
            $statement->bindValue(':title', $this->title);
            $statement->bindValue(':price', $this->price);
            $statement->bindValue(':description', $this->description);
            $statement->bindValue(':id', $this->id);
            $statement->bindValue(':type', $this->type);
            $statement->execute();

        }catch(Exception $e){
            die('Error updating note<br>'. $e->getMessage() );
        }
    }

}
    