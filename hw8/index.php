<?php
session_start();
if (empty ($_POST['valuta'])&& (empty ($_SESSION['valuta']))){
    $_SESSION['valuta']='uah'; 
}
elseif(!empty ($_POST['valuta'])) {
    $_SESSION['valuta']=
    $_POST['valuta'];
}

$valuta = [
    'uah'=>['name' => 'Гривна',
            'course' => 1,
    ],
    'usd'=>['name' => 'Доллар',
            'course' => 27.1,
    ],
    'eur'=> ['name' => 'Евро',
            'course' => 30.2,
    ],
 ];
$tovars = [
    ['title' => 'Арбуз', 'price_val' => '220.66', 'discount_type' => 'percent',/*тип скидки может быть percent или value*/ 'discount_val' => '20'],
    ['title' => 'Ананас', 'price_val' => '265', 'discount_type' => 'value','discount_val' => '19.99'],
    ['title' => 'Клубника', 'price_val' => '768', 'discount_type' => 'value','discount_val' => '55'],
    ['title' => 'Картофель', 'price_val' => '56', 'discount_type' => 'percent','discount_val' => '1.99'],
    ['title' => 'Свекла', 'price_val' => '399.99', 'discount_type' => 'value','discount_val' => '99.99'],
    ['title' => 'Баклажан', 'price_val' => '234', 'discount_type' => 'value','discount_val' => '31'],
    ['title' => 'Сливы', 'price_val' => '963', 'discount_type' => 'percent','discount_val' => '45.96'],
    ['title' => 'Бананы', 'price_val' => '345', 'discount_type' => 'value','discount_val' => '33'],
    ['title' => 'Дыня', 'price_val' => '789.90', 'discount_type' => 'percent','discount_val' => '29'],
    ['title' => 'Морковь', 'price_val' => '323.80', 'discount_type' => 'percent','discount_val' => '55'],
];
$course = $valuta[ $_SESSION['valuta'] ] [ 'course' ] ;


function getPriceWithDiscount($price, $discountType, $discount ){
    if ($discountType==='value'){
        $result=$price-$discount;
    }
    else {
        $result=$price-($price*$discount/100);
    }
    return round ($result,2);
}

function convertPrice($price, $course){    
    $result = $price/$course;
    return round ($result,2);
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Homework</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
</head>
<body>
    <form action="index.php" method="POST">
        <div>
            <select class="form-select" aria-label="Default select example" name="valuta" value=<?=$_POST['valuta']?> >
                <option <?php if($_SESSION['valuta'] === 'uah'):?>selected<?php endif;?> value="uah">Гривна</option>
                <option <?php if($_SESSION['valuta'] === 'usd'):?>selected<?php endif;?> value="usd">Доллар</option>
                <option <?php if($_SESSION['valuta'] === 'eur'):?>selected<?php endif;?> value="eur">Евро</option>
            </select>
            <button type="submit" class="btn btn-primary">Отправить</button>
        </div>
    </form>
    <h1><?=$_SESSION['valuta']?></h1>
    <div class="container">
        <div class="row">
            <table class="table col-sm-12">
                <thead>
                    <tr>
                        <th scope="col">Название товара</th>
                        <th scope="col">Цена товара</th>
                        <th scope="col">Тип скидки</th>
                        <th scope="col">Скидка</th>
                        <th scope="col">Цена со скидкой</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($tovars as $tovar): ?>
                    <tr>
                        <td><?= $tovar['title']?></td>
                        <td><?= convertPrice( $tovar['price_val'],$course)  ?></td>
                        <td><?= $tovar['discount_type']?></td>
                        <td><?= convertPrice( $tovar['discount_val'],$course) ?></td>
                        <td><?= convertPrice (getPriceWithDiscount($tovar['price_val'], $tovar['discount_type'], $tovar['discount_val']), $course) ?></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>



    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous">
    </script>

</body>
  
</html>